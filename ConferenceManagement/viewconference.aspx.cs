﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class viewconference : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindConference();
        }
    }
    protected void bindConference()
    {
        var con = db.Conferences.OrderBy(F => F.Date);
        gvConference.DataSource = con;
        gvConference.DataBind();
    }
    protected void gvConference_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void gvConference_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("addconference.aspx");
    }

    protected void gvConference_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}