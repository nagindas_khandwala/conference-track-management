﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    public Common()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string Base64Encode(string sValue)
    {
        try
        {
            byte[] strBytes = System.Text.Encoding.UTF8.GetBytes(sValue);
            return System.Convert.ToBase64String(strBytes);
        }
        catch (Exception ex)
        {
            return sValue;
        }
    }

    public static string Base64Decode(string sValue)
    {
        try
        {
            byte[] strBytes = System.Convert.FromBase64String(sValue);
            return System.Text.Encoding.UTF8.GetString(strBytes);
        }
        catch (Exception ex)
        {
            return sValue;
        }
    }

}