﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class addconference : System.Web.UI.Page
{
  ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    int ConferenceID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindRoom();
            bindEmployee();
            if ((Request.QueryString["id"] != null))
            {
                ConferenceID = Convert.ToInt32(Request.QueryString["id"]);
                ViewState["ConferenceID"] = ConferenceID;
                btnSave.Text = "Update";
                ltrTitle.Text = "Edit Conference";
                bindData();
            }
        }
    }
    protected void bindEmployee()
    {
        var emp = (from e in db.Employees select e);
        chkEmployee.DataSource = emp;
        chkEmployee.DataBind();
    }
    protected void bindData()
    {
        ConferenceID = (int)ViewState["ConferenceID"];
        var con = db.Conferences.SingleOrDefault(F => F.ConferenceID == ConferenceID);
        if (con != null)
        {
            DateTime sDate = (DateTime)con.Date;
            txtDate.Text = sDate.ToString("MM/dd/yyyy HH:mm");
            drpRoom.SelectedIndex = drpRoom.Items.IndexOf(drpRoom.Items.FindByValue(con.RoomID.ToString()));
            txtMessage.Text = con.Message;
            txtAboutTalk.Text = con.AboutTalk;
            txtBenefit.Text = con.Benefits;
            if (con.EmployeeConferences.Count > 0)
            {
                foreach (EmployeeConference i in con.EmployeeConferences)
                {
                    foreach (ListItem item in chkEmployee.Items)
                    {
                        int val = Convert.ToInt32(item.Value);
                        if (i.EmployeeID == val)
                        {
                            item.Selected = true;
                            continue;
                        }
                    }
                }
            }
        }
    }
    private void bindRoom()
    {
        var room = (from h in db.Rooms select h);
        if (room != null)
        {
            drpRoom.DataSource = room;
            drpRoom.DataBind();
        }
        drpRoom.Items.Insert(0, new ListItem(" - - Select Room - - ", "0"));
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)//Update
        {
            ConferenceID = (int)ViewState["ConferenceID"];
            var con = db.Conferences.SingleOrDefault(F => F.ConferenceID == ConferenceID);
            if (con != null)
            {
                con.Date = Convert.ToDateTime(txtDate.Text);
                con.RoomID = Convert.ToInt32(drpRoom.SelectedValue);
                con.Message = txtMessage.Text;
                con.AboutTalk = txtAboutTalk.Text;
                con.Benefits = txtBenefit.Text;
                db.SubmitChanges();

                db.EmployeeConferences.DeleteAllOnSubmit(con.EmployeeConferences);

                foreach (ListItem item in chkEmployee.Items)
                {
                    if (item.Selected == true)
                    {
                        EmployeeConference ec = new EmployeeConference();
                        ec.EmployeeID = Convert.ToInt32(item.Value);
                        ec.ConferenceID = ConferenceID;
                        db.EmployeeConferences.InsertOnSubmit(ec);
                        db.SubmitChanges();
                    }
                }

                Session["Message"] = "Conference has been updated successfully";
                Response.Redirect("viewconference.aspx");
            }
        }
        else
        {
            Conference con = new Conference();

            con.Date = Convert.ToDateTime(txtDate.Text);
            con.RoomID = Convert.ToInt32(drpRoom.SelectedValue);
            con.Message = txtMessage.Text;
            con.AboutTalk = txtAboutTalk.Text;
            con.Benefits = txtBenefit.Text;
            db.Conferences.InsertOnSubmit(con);
            db.SubmitChanges();

            foreach (ListItem item in chkEmployee.Items)
            {
                if (item.Selected == true)
                {
                    EmployeeConference ec = new EmployeeConference();
                    ec.EmployeeID = Convert.ToInt32(item.Value);
                    ec.ConferenceID = con.ConferenceID;
                    db.EmployeeConferences.InsertOnSubmit(ec);
                    db.SubmitChanges();
                }
            }
            Session["Message"] = "Conference has been inserted successfully";
            Response.Redirect("viewconference.aspx");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewconference.aspx");
    }
}