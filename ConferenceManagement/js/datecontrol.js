﻿$(function () {
    $('#cpContent_txtDOB').datetimepicker({
        minView: 2,
        format: 'mm/dd/yyyy',
        autoclose: true
    });
    
    $('#cpContent_txtDate').datetimepicker({
        format: 'mm/dd/yyyy HH:mm',
        autoclose: true
    });

});