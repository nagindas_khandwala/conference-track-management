﻿using System;
using System.Linq;

public partial class myprofile : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            if (Session["User"] != null)
            {
                bindEditData();
            }
        }                
    }

    protected void bindEditData() {
        int adminUserID = ((AdminUser)Session["User"]).AdminUserID;
        var user = db.AdminUsers.SingleOrDefault(f => f.AdminUserID == adminUserID);
        if (user != null)
        {
            txtName.Text = user.Name;
            txtPhone.Text = user.Phone;
            txtEmail.Text = user.Email;
            txtOldPassword.Text = Common.Base64Decode(user.Password);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            int adminUserID = ((AdminUser)Session["User"]).AdminUserID;

            var user = db.AdminUsers.SingleOrDefault(f => f.AdminUserID == adminUserID);
            if (user != null)
            {
                user.Name = txtName.Text.Trim();
                user.Phone = txtPhone.Text.Trim();
                user.Email = txtEmail.Text.Trim();
                db.SubmitChanges();
                lblMessage.Text = "Profile updated successfully";
            }
        }
        catch (Exception ex) {
            lblMessage.Text = ex.Message;
        }
            
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("home.aspx");
    }

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        try
        {
            string encryptedPassword = Common.Base64Encode(txtOldPassword.Text.Trim());
            int adminUserID = ((AdminUser)Session["User"]).AdminUserID;
            var user = db.AdminUsers.SingleOrDefault(f => f.AdminUserID == adminUserID);
            //update
            if (user != null)
            {
                //check for current password
                if (user.Password != encryptedPassword)
                {
                    lblMessage.Text = "Current password is incorrect";
                    return;
                }

                user.Password = Common.Base64Encode(txtNewPassword.Text.Trim());
                db.SubmitChanges();
                lblMessage.Text = "Password has been updated successfully";

                bindEditData();
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }

    }
}