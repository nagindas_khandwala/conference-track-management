﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class viewusers : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            bindUser();
        }
    }

    protected void bindUser() {
        var users = db.AdminUsers.Where(F => F.IsDeleted== false).OrderBy(F => F.Name);
        gvUsers.DataSource = users;
        gvUsers.DataBind();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("adduser.aspx");
    }

    protected void gvUsers_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "DeleteUser")
            {
                int id = (int)e.CommandArgument;
                var user = db.AdminUsers.SingleOrDefault(F => F.AdminUserID == id);
                if (user != null)
                {
                    user.IsDeleted = true;
                    user.IsActive = false;
                    db.SubmitChanges();
                    bindUser();
                    lblMessage.Text = "User has been deleted successfully";
                }
            }
        }
        catch (Exception ex) {
            lblMessage.Text = ex.Message;
        }
    }

    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsers.PageIndex = e.NewPageIndex;
        bindUser();
    }

    protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow) {
            AdminUser user = (AdminUser)e.Row.DataItem;
            if (user.IsAdmin==true) {
                e.Row.FindControl("imgDelete").Visible = false; 
            }
        }
    }
}