﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myprofile.aspx.cs" Inherits="myprofile" MasterPageFile="~/ui.master" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <asp:ValidationSummary ID="vldSummary" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    <asp:ValidationSummary ID="vldSummaryCP" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="CP"></asp:ValidationSummary>
    <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
        <tr>
            <td class="Headeline" align="left" width="20%">
                <h1>My Profile</h1>
            </td>
            <td width="60%" align="center" class="Headeline">
                <AD:MSG ID="message" runat="server" />
                <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
            </td>
            <td align="right" width="20%" class="Headeline"></td>
        </tr>
        <tr>
            <td align="center" colspan="3" height="20px;"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table border="0" align="left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="50%" valign="top">
                            <table border="0" align="left" cellpadding="5" cellspacing="5" width="100%">
                                <tr>
                                    <td align="right" width="20%">
                                        <span style="color: red;">*</span>&nbsp;Name:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" CssClass="inputcontrol" runat="server" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="vldNameReq" ControlToValidate="txtName" runat="server"
                                            Display="None" ErrorMessage="Name is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Phone:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPhone" CssClass="inputcontrol" runat="server" MaxLength="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Email:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" CssClass="inputcontrol" runat="server" Width="200px" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="vldEmailReq" ControlToValidate="txtEmail" runat="server"
                                            Display="None" ErrorMessage="Email is required" />
                                        <asp:RegularExpressionValidator ID="vldEmailReg" runat="server" ErrorMessage="Invalid Email address"
                                            Display="None" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td align="right">
                                        Auction Properties:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkAuction" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Custom Properties:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkCustom" runat="server" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="btnSave" BorderStyle="None" runat="server" Text="Save" Font-Bold="true"
                                            CssClass="button" OnClick="btnSave_Click"></asp:Button>
                                        <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                                            CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table border="0" align="left" cellpadding="5" cellspacing="5" width="100%">
                                <tr>
                                    <td align="right" width="20%">
                                        <span style="color: red;">*</span>&nbsp;Old Password:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOldPassword" CssClass="inputcontrol" runat="server" TextMode="Password"
                                            AutoCompleteType="None" />
                                        <asp:RequiredFieldValidator ID="vldOldPasswordReq" ControlToValidate="txtOldPassword"
                                            runat="server" Display="None" ErrorMessage="Old Password is required" ValidationGroup="CP" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;New Password:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNewPassword" CssClass="inputcontrol" runat="server" TextMode="Password"
                                            AutoCompleteType="None" />
                                        <asp:RequiredFieldValidator ID="vldNewPasswordReq" ControlToValidate="txtNewPassword"
                                            runat="server" Display="None" ErrorMessage="New Password is required" ValidationGroup="CP" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Confirm Password:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtConfirmPassword" CssClass="inputcontrol" runat="server" TextMode="Password"
                                            AutoCompleteType="None" />
                                        <asp:RequiredFieldValidator ID="vldConfirmPasswordReq" runat="server" ControlToValidate="txtConfirmPassword"
                                            Display="None" ErrorMessage="Confirm Password is required" ValidationGroup="CP"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="vldConfirmPasswordCmp" runat="server" ControlToValidate="txtConfirmPassword"
                                            Display="None" ErrorMessage='Confirm Password must match with password' ControlToCompare="txtNewPassword" ValidationGroup="CP"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                         <asp:Button ID="btnChangePassword" BorderStyle="None" runat="server" Text="Change Password" Font-Bold="true"
                                            CssClass="button" OnClick="btnChangePassword_Click" ValidationGroup="CP"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
