﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class viewemployee : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindEmployee();
        }
    }
    protected void bindEmployee()
    {
        var emp = db.Employees.OrderBy(F => F.Name);
        gvEmployee.DataSource = emp;
        gvEmployee.DataBind();
    }
    protected void gvEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "DeleteUser")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                var emp = db.Employees.SingleOrDefault(F => F.EmployeeID == id);
                if (emp != null)
                {
                    db.Employees.DeleteOnSubmit(emp);
                    db.SubmitChanges();
                    bindEmployee();
                    lblMessage.Text = "Employee has been deleted successfully";
                }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }
    protected void gvEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmployee.PageIndex = e.NewPageIndex;
        bindEmployee();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("addemployee.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
}