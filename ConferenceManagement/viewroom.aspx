﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui.master" AutoEventWireup="true" CodeFile="viewroom.aspx.cs" Inherits="viewroom" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <asp:ValidationSummary ID="vldSummary" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    <asp:UpdatePanel ID="upContent" runat="server">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
                <tr>
                    <td class="Headeline" align="left" width="20%">
                        <h1>
                            <asp:Literal ID="ltrTitle" Text="Rooms" runat="server"></asp:Literal>
                        </h1>
                    </td>
                    <td width="60%" align="center" class="Headeline">
                        <AD:MSG ID="message" runat="server" />
                        <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
                    </td>
                    <td align="right" width="20%" class="Headeline">
                        <asp:Button ID="btnBack" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                            CausesValidation="False" Text="Back" OnClick="btnBack_Click"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" height="20px;"></td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                            <tr>
                                <td width="50%">
                                    <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                                        <tr>
                                            <td align="right" width="45%">
                                                <span style="color: red;">*</span>&nbsp;Name:
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtName" CssClass="inputcontrol" runat="server" MaxLength="50" />
                                                <asp:RequiredFieldValidator ID="vldNameReq" ControlToValidate="txtName" runat="server"
                                                    Display="None" ErrorMessage="Name is required" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="45%"></td>
                                            <td align="left">
                                                <asp:Button ID="btnSave" BorderStyle="None" runat="server" Text="Save" Font-Bold="true"
                                                    CssClass="button" OnClick="btnSave_Click"></asp:Button>
                                                <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                                                    CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                                        <tr>
                                            <td>
                                                <asp:GridView runat="server" ID="gvRoom" AutoGenerateColumns="false" AllowPaging="false"
                                                    AllowSorting="false" Width="100%" GridLines="Horizontal" CellPadding="5" CssClass="grid"
                                                    PageSize="10" OnRowCommand="gvRoom_RowCommand" OnPageIndexChanging="gvRoom_PageIndexChanging">
                                                    <EmptyDataRowStyle CssClass="gridempty" />
                                                    <EmptyDataTemplate>
                                                        <h3>No room found
                                                        </h3>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="gridheadercustom"></HeaderStyle>
                                                    <RowStyle CssClass="griditem" />
                                                    <AlternatingRowStyle CssClass="gridaltitem" />
                                                    <PagerStyle CssClass="gridpager"></PagerStyle>
                                                    <Columns>
                                                        <asp:TemplateField AccessibleHeaderText="Name">
                                                            <HeaderTemplate>
                                                                Name
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <%# Eval("Name")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgEdit" AlternateText="Edit" runat="server" ImageUrl="img/admin/edit.gif"
                                                                    CommandName="EditRoom" CommandArgument='<%# Eval("RoomID")  %>' ToolTip="Edit" CausesValidation="false"></asp:ImageButton>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgDelete" AlternateText="Delete" runat="server"
                                                                    ImageUrl="img/admin/delete.gif" CommandName="DeleteRoom" CommandArgument='<%# Eval("RoomID")  %>'
                                                                    ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this room?')"
                                                                    CausesValidation="false"></asp:ImageButton>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="Background">
            </div>
            <div id="Progress">
                <img src="img/admin/loading.gif" alt="" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

