﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="viewusers.aspx.cs" Inherits="viewusers" MasterPageFile="~/ui.master" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <asp:UpdatePanel ID="upContent" runat="server">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
                <tr>
                    <td class="Headeline" align="left" width="20%">
                        <h1>Users</h1>
                    </td>
                    <td width="60%" align="center" class="Headeline">
                        <AD:MSG ID="message" runat="server" />
                        <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
                    </td>
                    <td align="right" width="20%" class="Headeline">
                        <asp:Button ID="btnAdd" BorderStyle="None" runat="server" Text="Add" Font-Bold="true"
                            CssClass="button" TabIndex="2" OnClick="btnAdd_Click"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" height="20px;"></td>
                </tr>
                <tr>
                    <td colspan="3" valign="top" height="475" style="padding-top: 5px;">
                        <asp:GridView runat="server" ID="gvUsers" AutoGenerateColumns="false" AllowPaging="false"
                            AllowSorting="false" Width="100%" GridLines="Horizontal" CellPadding="5" CssClass="grid"
                            PageSize="20" OnRowCommand="gvUsers_RowCommand" OnPageIndexChanging="gvUsers_PageIndexChanging"
                            OnRowDataBound="gvUsers_RowDataBound">
                            <EmptyDataRowStyle CssClass="gridempty" />
                            <EmptyDataTemplate>
                                <h3>No users found
                                </h3>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="gridheadercustom"></HeaderStyle>
                            <RowStyle CssClass="griditem" />
                            <AlternatingRowStyle CssClass="gridaltitem" />
                            <PagerStyle CssClass="gridpager"></PagerStyle>
                            <Columns>
                                <asp:TemplateField AccessibleHeaderText="Name">
                                    <HeaderTemplate>
                                        Name
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField AccessibleHeaderText="Username">
                                    <HeaderTemplate>
                                        Username
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Username")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField AccessibleHeaderText="Email">
                                    <HeaderTemplate>
                                        Email
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Email")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField AccessibleHeaderText="Phone">
                                    <HeaderTemplate>
                                        Phone
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Phone")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Admin">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAdmin" Enabled="false" runat="server" Checked='<%# Eval("IsAdmin") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Active">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" Enabled="false" runat="server" Checked='<%# Eval("IsActive") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" AlternateText="Edit" runat="server" ImageUrl="img/admin/edit.gif"
                                            PostBackUrl='<%# "adduser.aspx?id=" + Eval("AdminUserID")%>' ToolTip="Edit" CausesValidation="false"></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" AlternateText="Delete" runat="server"
                                            ImageUrl="img/admin/delete.gif" CommandName="DeleteUser" CommandArgument='<%# Eval("AdminUserID")  %>'
                                            ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this user?')"
                                            CausesValidation="false"></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="Background">
            </div>
            <div id="Progress">
                <img src="images/loading.gif" alt="" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
