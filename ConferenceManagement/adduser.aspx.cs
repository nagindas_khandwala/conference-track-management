﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adduser : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    int userID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((Request.QueryString["id"] != null))
            {
                userID = Convert.ToInt32(Request.QueryString["id"]);
                ViewState["UserID"] = userID;
                btnSave.Text = "Update";
                ltrTitle.Text = "Edit User";
                bindData();
            }
        }
    }


    protected void bindData()
    {
        userID = (int)ViewState["UserID"];
        var user = db.AdminUsers.SingleOrDefault(F => F.AdminUserID == userID);
        if (user != null)
        {
            txtName.Text = user.Name;
            txtPhone.Text = user.Phone;
            txtEmail.Text = user.Email;
            txtUserName.Text = user.Username;
            txtPassword.Attributes.Add("value", Common.Base64Decode(user.Password));
            txtConfirmPassword.Attributes.Add("value", Common.Base64Decode(user.Password));
            chkActive.Checked = user.IsActive.Value;

            if (user.IsAdmin == true)
            {
                chkActive.Enabled = false;
            }            
        }
    }
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["UserID"] != null)//Update
        {
            userID = (int)ViewState["UserID"];
            var userAlreadyExits = db.AdminUsers.Where(F => F.Username == txtUserName.Text.Trim() & F.AdminUserID != userID & F.IsDeleted==false);
            if (userAlreadyExits.Count() > 0)
            {
                Session["Message"] = "This username already exists, please try with another username";
                return;
            }
            var user = db.AdminUsers.SingleOrDefault(F => F.AdminUserID == userID);
            if (user != null)
            {
                user.Name = txtName.Text;
                user.Phone = txtPhone.Text;
                user.Email = txtEmail.Text;
                user.Username = txtUserName.Text;
                user.Password = Common.Base64Encode(txtPassword.Text.Trim());
                user.IsActive = chkActive.Checked;                
                db.SubmitChanges();
                Session["Message"] = "User has been updated successfully";
                Response.Redirect("viewusers.aspx");
            }
        }
        else
        {
            AdminUser user = new AdminUser();
            var userAlreadyExits = db.AdminUsers.Where(F => F.Username == txtUserName.Text.Trim() & F.IsDeleted == false);
            if (userAlreadyExits.Count() > 0)
            {
                Session["Message"] = "This username already exists, please try with another username";
                return;
            }
            user.Name = txtName.Text;
            user.Phone = txtPhone.Text;
            user.Email = txtEmail.Text;
            user.Username = txtUserName.Text;
            user.Password = Common.Base64Encode(txtPassword.Text.Trim());
            user.IsAdmin = false;
            user.IsActive = chkActive.Checked;
            user.IsDeleted = false;
            db.AdminUsers.InsertOnSubmit(user);
            db.SubmitChanges();
            Session["Message"] = "User has been inserted successfully";
            Response.Redirect("viewusers.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewusers.aspx");
    }
}