﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="adduser.aspx.cs" Inherits="adduser" MasterPageFile="~/ui.master" %>
<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <asp:ValidationSummary ID="vldSummary" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
        <tr>
            <td class="Headeline" align="left" width="20%">
                <h1>
                    <asp:Literal ID="ltrTitle" Text="Add User" runat="server"></asp:Literal>
                </h1>
            </td>
            <td width="60%" align="center" class="Headeline">
                <AD:MSG ID="message" runat="server" />
                <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
            </td>
            <td align="right" width="20%" class="Headeline">
                <asp:Button ID="btnSave" BorderStyle="None" runat="server" Text="Save" Font-Bold="true"
                    CssClass="button" OnClick="btnSave_Click" ></asp:Button>
                <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                    CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click" ></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" height="20px;"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table border="0" align="left" cellpadding="5" cellspacing="5" width="80%">
                    <tr>
                        <td width="50%" valign="top">
                            <table border="0" align="left" cellpadding="5" cellspacing="5" width="100%">
                                <tr>
                                    <td align="right" width="45%">
                                        <span style="color: red;">*</span>&nbsp;Name:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" CssClass="inputcontrol" runat="server" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="vldNameReq" ControlToValidate="txtName" runat="server"
                                            Display="None" ErrorMessage="Name is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Phone:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhone" CssClass="inputcontrol" runat="server" MaxLength="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Email:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" CssClass="inputcontrol" runat="server" MaxLength="50" Width="200px" />
                                        <asp:RequiredFieldValidator ID="vldEmailReq" ControlToValidate="txtEmail" runat="server"
                                            Display="None" ErrorMessage="Email is required" />
                                        <asp:RegularExpressionValidator ID="vldEmailReg" runat="server" ErrorMessage="Email is invalid"
                                            Display="None" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table border="0" align="left" cellpadding="5" cellspacing="5" width="100%">
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Username:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUserName" CssClass="inputcontrol" runat="server" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="vldUserNameReq" ControlToValidate="txtUserName" runat="server"
                                            Display="None" ErrorMessage="Username is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Password:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPassword" CssClass="inputcontrol" runat="server" MaxLength="50"
                                            TextMode="Password" />
                                        <asp:RequiredFieldValidator ID="vldPasswordReq" ControlToValidate="txtPassword" runat="server"
                                            Display="None" ErrorMessage="Password is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Confirm Password:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtConfirmPassword" CssClass="inputcontrol" runat="server" MaxLength="50"
                                            TextMode="Password" />
                                        <asp:RequiredFieldValidator ID="vldConfirmReq" ControlToValidate="txtConfirmPassword"
                                            runat="server" Display="None" ErrorMessage="Confirm password is required" />
                                        <asp:CompareValidator ID="vldConfirmCmp" ControlToValidate="txtConfirmPassword" runat="server"
                                            Display="None" ErrorMessage="Confirm password must be same as password" ControlToCompare="txtPassword" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Active:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

