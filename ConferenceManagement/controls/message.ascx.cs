﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_message : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        if (Session["Message"]!=null)
        {
            string outputstr = "" + "<table border=0 cellspacing=0 cellpadding=1 width=100%><tr><td align=Center class=msg>" + Session["Message"].ToString() + "</td></tr></table>";
            writer.Write(outputstr);
            Session["Message"] = null;
        }
    }
}