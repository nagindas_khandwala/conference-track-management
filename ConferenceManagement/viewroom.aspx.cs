﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class viewroom : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindRoom();
        }
    }

    protected void bindRoom()
    {
        var room = (from r in db.Rooms orderby r.Name select r);
        gvRoom.DataSource = room;
        gvRoom.DataBind();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ViewState["RoomID"] != null)
        {
            int roomID = (int)ViewState["RoomID"];
            var roomAlreadyExits = db.Rooms.Where(F => F.Name == txtName.Text.Trim() & F.RoomID != roomID);
            if (roomAlreadyExits.Count() > 0)
            {
                Session["Message"] = "This room already exists, please try with another name";
                return;
            }
            var room = db.Rooms.SingleOrDefault(f => f.RoomID == roomID);
            if(room != null)
            {
                room.Name = txtName.Text.Trim();
                db.SubmitChanges();
                bindRoom();
                Session["Message"] = "Room has been updated successfully";
            }
        }
        else
        {
            Room r = new Room();
            var roomAlreadyExits = db.Rooms.Where(F => F.Name == txtName.Text.Trim());
            if (roomAlreadyExits.Count() > 0)
            {
                Session["Message"] = "This room already exists, please try with another name";
                return;
            }
            r.Name = txtName.Text.Trim();
            db.Rooms.InsertOnSubmit(r);
            db.SubmitChanges();
            bindRoom();
            Session["Message"] = "Room has been inserted successfully";
        }
        txtName.Text = string.Empty;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtName.Text = null;
        ViewState["RoomID"] = null;
    }

    protected void gvRoom_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "EditRoom")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                var room = db.Rooms.SingleOrDefault(F => F.RoomID == id);
                if (room != null)
                {
                    ViewState["RoomID"] = id;
                    txtName.Text = room.Name;
                }
            }
            if (e.CommandName == "DeleteRoom")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                var room = db.Rooms.SingleOrDefault(F => F.RoomID == id);
                if (room != null)
                {
                    db.Rooms.DeleteOnSubmit(room);
                    db.SubmitChanges();
                    bindRoom();
                    lblMessage.Text = "Room has been deleted successfully";
                }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }

    protected void gvRoom_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRoom.PageIndex = e.NewPageIndex;
        bindRoom();
    }
}