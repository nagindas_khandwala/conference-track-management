﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui.master" AutoEventWireup="true" CodeFile="viewemployee.aspx.cs" Inherits="viewemployee" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
        <tr>
            <td class="Headeline" align="left" width="20%">
                <h1>
                    <asp:Literal ID="ltrTitle" Text="Employees" runat="server"></asp:Literal>
                </h1>
            </td>
            <td width="60%" align="center" class="Headeline">
                <AD:MSG ID="message" runat="server" />
                <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
            </td>
            <td align="right" width="20%" class="Headeline">
                <asp:Button ID="btnAdd" BorderStyle="None" runat="server" Text="Add" Font-Bold="true"
                    CssClass="button" OnClick="btnAdd_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                    CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" height="20px;"></td>
        </tr>
        <tr>
            <td width="50%" colspan="3">
                <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                    <tr>
                        <td>
                            <asp:GridView runat="server" ID="gvEmployee" AutoGenerateColumns="false" AllowPaging="false"
                                AllowSorting="false" Width="100%" GridLines="Horizontal" CellPadding="5" CssClass="grid"
                                PageSize="20" OnRowCommand="gvEmployee_RowCommand" OnPageIndexChanging="gvEmployee_PageIndexChanging">
                                <EmptyDataRowStyle CssClass="gridempty" />
                                <EmptyDataTemplate>
                                    <h3>No employee found
                                    </h3>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="gridheadercustom"></HeaderStyle>
                                <RowStyle CssClass="griditem" />
                                <AlternatingRowStyle CssClass="gridaltitem" />
                                <PagerStyle CssClass="gridpager"></PagerStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Name
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Name")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            DOB
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("DOB","{0:MM/dd/yyyy}")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="8%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Address
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Address")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            City
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("City")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            State
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("State")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Email
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Email")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Phone
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Phone")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="8%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" AlternateText="Edit" runat="server" ImageUrl="img/admin/edit.gif"
                                                 PostBackUrl='<%# "addemployee.aspx?id=" + Eval("EmployeeID")%>' ToolTip="Edit" CausesValidation="false"></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" AlternateText="Delete" runat="server"
                                                ImageUrl="img/admin/delete.gif" CommandName="DeleteEmployee" CommandArgument='<%# Eval("EmployeeID")  %>'
                                                ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this employee?')"
                                                CausesValidation="false"></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

