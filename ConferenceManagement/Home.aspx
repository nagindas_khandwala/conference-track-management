﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="Home" MasterPageFile="~/ui.master" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="0" cellpadding="0" width="25%" border="0" align="center" class="">
        <tr style="height: 50px;">
            <td></td>
        </tr>
        <tr>
            <td valign="top" height="90">
                <table width="100%" border="0" align="center" cellpadding="5" cellspacing="5" class="dashboard tableBorder">
                    <tr>
                        <td bgcolor="black" class="gridtopBG" colspan="4">
                            <strong>
                                Dashboard</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="10px;" colspan="4"></td>
                    </tr>
                    
                    <tr>
                        <td valign="top" align="right" width="60%">Total Rooms
                        </td>
                        <td align="left" valign="top" width="10%">
                            <strong>: </strong>
                        </td>
                        <td valign="top" width="10%" align="right">
                            <asp:Label ID="lblTotalRooms" runat="server"></asp:Label>
                        </td>
                        <td></td>
                    </tr>                    
                    <tr>
                        <td valign="top" align="right">Total Employees
                        </td>
                        <td align="left" valign="top">
                            <strong>: </strong>
                        </td>
                        <td valign="top" width="10%" align="right">
                            <asp:Label ID="lblTotalEmployees" runat="server"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">Total Conferences
                        </td>
                        <td align="left" valign="top">
                            <strong>: </strong>
                        </td>
                        <td valign="top" width="10%" align="right">
                            <asp:Label ID="lblTotalConferences" runat="server"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                     
                    <tr>
                        <td height="10px;" colspan="4"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 50px;">
            <td></td>
        </tr>
    </table>
</asp:Content>

