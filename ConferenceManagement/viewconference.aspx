﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui.master" AutoEventWireup="true" CodeFile="viewconference.aspx.cs" Inherits="viewconference" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
        <tr>
            <td class="Headeline" align="left" width="20%">
                <h1>
                    <asp:Literal ID="ltrTitle" Text="Conference" runat="server"></asp:Literal>
                </h1>
            </td>
            <td width="60%" align="center" class="Headeline">
                <AD:MSG ID="message" runat="server" />
                <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
            </td>
            <td align="right" width="20%" class="Headeline">
                <asp:Button ID="btnAdd" BorderStyle="None" runat="server" Text="Add" Font-Bold="true"
                    CssClass="button" OnClick="btnAdd_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                    CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" height="20px;"></td>
        </tr>
        <tr>
            <td width="50%" colspan="3">
                <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                    <tr>
                        <td>
                            <asp:GridView runat="server" ID="gvConference" AutoGenerateColumns="false" AllowPaging="false"
                                AllowSorting="false" Width="100%" GridLines="Horizontal" CellPadding="5" CssClass="grid"
                                PageSize="20" OnRowCommand="gvConference_RowCommand" OnPageIndexChanging="gvConference_PageIndexChanging" OnSelectedIndexChanged="gvConference_SelectedIndexChanged">
                                <EmptyDataRowStyle CssClass="gridempty" />
                                <EmptyDataTemplate>
                                    <h3>No conference found
                                    </h3>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="gridheadercustom"></HeaderStyle>
                                <RowStyle CssClass="griditem" />
                                <AlternatingRowStyle CssClass="gridaltitem" />
                                <PagerStyle CssClass="gridpager"></PagerStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Date Time
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Date","{0:MM/dd/yyyy HH:mm}")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="12%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Room
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Room.Name")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Message
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Message")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            AboutTalk
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("AboutTalk")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Benefit
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("Benefits")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" AlternateText="Edit" runat="server" ImageUrl="img/admin/edit.gif"
                                                PostBackUrl='<%# "addconference.aspx?id=" + Eval("ConferenceID")%>' ToolTip="Edit" CausesValidation="false"></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" AlternateText="Delete" runat="server"
                                                ImageUrl="img/admin/delete.gif" CommandName="DeleteEmployee" CommandArgument='<%# Eval("ConferenceID")  %>'
                                                ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this conference?')"
                                                CausesValidation="false"></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

