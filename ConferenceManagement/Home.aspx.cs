﻿using System;
using System.Linq;

public partial class Home : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack){
            lblTotalRooms.Text = db.Rooms.Count().ToString();
            lblTotalEmployees.Text = db.Employees.Count().ToString();
            lblTotalConferences.Text = db.Conferences.Count().ToString();
        }
    }
}