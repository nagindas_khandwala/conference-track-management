﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conference Management</title>
    <link href="css/styleadmin.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ValidationSummary ID="vldSummary" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr style="height: 100px;">
                <td class="topbg" rowspan="2"></td>
                <td width="80%" class="headerbg" align="left" valign="middle">
                    <h1 style="font-size: 32px;">
                        <strong>
                            Conference Management
                        </strong>
                    </h1>
                </td>
                <td class="topbg" rowspan="2"></td>
            </tr>
            <tr>
                <td class="menubg">&nbsp;
                </td>
            </tr>
            <tr>
                <td class="middlebg" rowspan="2"></td>
                <td>
                    <div class="middletable">
                        <table cellspacing="0" cellpadding="0" width="28%" border="0" align="center">
                            <tr>
                                <td height="50px"></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td align="center">
                                    <asp:Label ID="lblmsg" runat="server" CssClass="msg"></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table cellspacing="5" cellpadding="5" width="100%" class="loginbox">
                                        <tr style="height: 30px;">
                                            <td colspan="2" align="center" class="gridtopBG whitefont">
                                                <strong>Login to Continue...</strong>
                                            </td>
                                        </tr>
                                        <tr style="height: 15px;">
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td width="40%" align="right">
                                                <strong>Username : &nbsp;</strong>
                                            </td>
                                            <td width="60%">
                                                <asp:TextBox ID="txtUsername" TabIndex="0" runat="server" Width="140px" MaxLength="50"
                                                    CssClass="inputcontrol"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldUsernameReq" runat="server" ControlToValidate="txtUsername"
                                                    ErrorMessage="Username is required" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <strong>Password : &nbsp;</strong>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPassword" TabIndex="0" runat="server" Width="140px" MaxLength="50"
                                                    TextMode="Password" CssClass="inputcontrol"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldPasswordReq" runat="server" ControlToValidate="txtPassword"
                                                    ErrorMessage="Password is required" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="padding-top: 10px;">
                                                <asp:Button ID="btnLogin" Text="Login" runat="server" CssClass="button" OnClick="btnLogin_Click"/>&nbsp;
                                                <asp:LinkButton ID="lnkForgotPassword" runat="server" PostBackUrl="~/forgotpassword.aspx" Text="Forgot Password?"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class="middlebg" rowspan="2"></td>
            </tr>
            <tr>
                <td height="30px" class="middlebg Copyright bottomtable" align="center">&copy; Copyright
                <%= DateTime.Now.Year.ToString()%>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
