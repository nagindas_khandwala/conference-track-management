﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class forgotpassword : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        txtUsername.Focus();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Response.Redirect("login.aspx");
    }
}