﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        txtUsername.Focus();
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string encryptedPassword = Common.Base64Encode(txtPassword.Text);
        string username = txtUsername.Text;

        dynamic userData = db.AdminUsers.SingleOrDefault(f => f.Username == username & f.Password == encryptedPassword & f.IsActive == true);
        if ((userData != null))
        {
            Session["User"] = userData;
            if ((Request.QueryString["page"] != null) && !string.IsNullOrEmpty(Request.QueryString["page"]))
            {
                Response.Redirect(Request.QueryString["page"]);
            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }
        else
        {
            lblmsg.Text = "Login failed, please try again.";
        }
    }
}