﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class addemployee : System.Web.UI.Page
{
    ConferenceManagementDataContext db = new ConferenceManagementDataContext();
    int EmployeeID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((Request.QueryString["id"] != null))
            {
                EmployeeID = Convert.ToInt32(Request.QueryString["id"]);
                ViewState["EmployeeID"] = EmployeeID;
                btnSave.Text = "Update";
                ltrTitle.Text = "Edit Employee";
                bindData();
            }
        }
    }
    protected void bindData()
    {
        EmployeeID = (int)ViewState["EmployeeID"];
        var emp = db.Employees.SingleOrDefault(F => F.EmployeeID == EmployeeID);
        if (emp != null)
        {
            txtName.Text = emp.Name;
            DateTime sDate = (DateTime)emp.DOB;
            txtDOB.Text = sDate.ToString("MM/dd/yyyy");
            txtAddress.Text = emp.Address;
            txtCity.Text = emp.City;
            txtState.Text = emp.State;
            txtEmail.Text = emp.Email;
            txtPhone.Text = emp.Phone;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)//Update
        {
            EmployeeID = (int)ViewState["EmployeeID"];
            var empAlreadyExits = db.Employees.Where(F => F.Name == txtName.Text.Trim() & F.EmployeeID != EmployeeID);
            if (empAlreadyExits.Count() > 0)
            {
                Session["Message"] = "This employee already exists, please try with another name";
                return;
            }
            var emp = db.Employees.SingleOrDefault(F => F.EmployeeID == EmployeeID);
            if (emp != null)
            {
                emp.Name = txtName.Text;
                emp.DOB = Convert.ToDateTime(txtDOB.Text);
                emp.Address = txtAddress.Text;
                emp.City = txtCity.Text;
                emp.State = txtState.Text;
                emp.Phone = txtPhone.Text;
                emp.Email = txtEmail.Text;
                db.SubmitChanges();
                Session["Message"] = "Employee has been updated successfully";
                Response.Redirect("viewemployee.aspx");
            }
        }
        else
        {
            Employee emp = new Employee();
            var empAlreadyExits = db.Employees.Where(F => F.Name == txtName.Text.Trim());
            if (empAlreadyExits.Count() > 0)
            {
                Session["Message"] = "This employee already exists, please try with another name";
                return;
            }
            emp.Name = txtName.Text;
            emp.DOB = Convert.ToDateTime(txtDOB.Text);
            emp.Address = txtAddress.Text;
            emp.City = txtCity.Text;
            emp.State = txtState.Text;
            emp.Phone = txtPhone.Text;
            emp.Email = txtEmail.Text;
            db.Employees.InsertOnSubmit(emp);
            db.SubmitChanges();
            Session["Message"] = "Employee has been inserted successfully";
            Response.Redirect("viewemployee.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewemployee.aspx");
    }
}