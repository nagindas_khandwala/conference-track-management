﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui.master" AutoEventWireup="true" CodeFile="addconference.aspx.cs" Inherits="addconference" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="js/bootstrap-datetimepicker.js"></script>
    <script src="js/datecontrol.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <asp:ValidationSummary ID="vldSummary" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
        <tr>
            <td class="Headeline" align="left" width="30%">
                <h1>
                    <asp:Literal ID="ltrTitle" Text="Add Conference" runat="server"></asp:Literal>
                </h1>
            </td>
            <td width="60%" align="center" class="Headeline">
                <AD:MSG ID="message" runat="server" />
                <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
            </td>
            <td align="right" width="20%" class="Headeline">
                <asp:Button ID="btnSave" BorderStyle="None" runat="server" Text="Save" Font-Bold="true"
                    CssClass="button" OnClick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                    CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" height="20px;"></td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                    <tr>
                        <td width="50%">
                            <table border="0" align="left" cellpadding="5" cellspacing="5" width="100%">
                                <tr>
                                    <td align="right" width="15%">Date:
                                    </td>
                                    <td height="50px">
                                        <asp:TextBox ID="txtDate" CssClass="form-control datetimepicker" runat="server" MaxLength="50" Width="300px" />
                                        <asp:RequiredFieldValidator ID="vldDateReq" ControlToValidate="txtDate" runat="server"
                                            Display="None" ErrorMessage="Date is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Room:
                                    </td>
                                    <td height="50px">
                                        <asp:DropDownList ID="drpRoom" runat="server" DataTextField="Name" DataValueField="RoomID" AppendDataBoundItems="true"
                                           CssClass="form-control" Width="300px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="vldRoomReq" ControlToValidate="drpRoom" runat="server"
                                            Display="None" ErrorMessage="Room is required" InitialValue="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Message:
                                    </td>
                                    <td height="50px">
                                        <asp:TextBox ID="txtMessage" CssClass="form-control" runat="server" Width="300px"
                                            TextMode="MultiLine" Rows="5" />
                                        <asp:RequiredFieldValidator ID="vldMessageReq" ControlToValidate="txtMessage" runat="server"
                                            Display="None" ErrorMessage="Message is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;About Talk:
                                    </td>
                                    <td height="50px">
                                        <asp:TextBox ID="txtAboutTalk" CssClass="form-control" runat="server" Width="300px"
                                            TextMode="MultiLine" Rows="5"  />
                                        <asp:RequiredFieldValidator ID="vldAboutTalkReq" ControlToValidate="txtAboutTalk" runat="server"
                                            Display="None" ErrorMessage="About talk is required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <span style="color: red;">*</span>&nbsp;Benefit:
                                    </td>
                                    <td height="50px">
                                        <asp:TextBox ID="txtBenefit" CssClass="form-control" runat="server" Width="300px"
                                            TextMode="MultiLine" Rows="5"  />
                                        <asp:RequiredFieldValidator ID="vldBenefitReq" ControlToValidate="txtBenefit" runat="server"
                                            Display="None" ErrorMessage="Benefit is required" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table border="0" align="left" cellpadding="5" cellspacing="5" width="100%">
                                <tr>
                                    <td width="15%">Employee:</td>
                                    <td>
                                        <asp:CheckBoxList ID="chkEmployee" runat="server" CssClass="checkbox" DataTextField="Name" DataValueField="EmployeeID">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>

