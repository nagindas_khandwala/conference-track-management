﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ui.master" AutoEventWireup="true" CodeFile="addemployee.aspx.cs" Inherits="addemployee" %>

<%@ Register TagPrefix="AD" TagName="MSG" Src="~/controls/message.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="js/bootstrap-datetimepicker.js"></script>
    <script src="js/datecontrol.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <asp:ValidationSummary ID="vldSummary" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
    <table cellspacing="0" cellpadding="0" border="0" width="98%" align="center">
        <tr>
            <td class="Headeline" align="left" width="20%">
                <h1>
                    <asp:Literal ID="ltrTitle" Text="Add Employee" runat="server"></asp:Literal>
                </h1>
            </td>
            <td width="60%" align="center" class="Headeline">
                <AD:MSG ID="message" runat="server" />
                <asp:Label ID="lblMessage" runat="server" CssClass="msg"></asp:Label>
            </td>
            <td align="right" width="20%" class="Headeline">
                <asp:Button ID="btnSave" BorderStyle="None" runat="server" Text="Save" Font-Bold="true"
                    CssClass="button" OnClick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" BorderStyle="None" Font-Bold="true" CssClass="button"
                    CausesValidation="False" Text="Cancel" OnClick="btnCancel_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" height="20px;"></td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table border="0" align="left" cellpadding="5" cellspacing="5" width="98%">
                    <tr>
                        <td align="right" width="45%">
                            <span style="color: red;">*</span>&nbsp;Name:
                        </td>
                        <td align="left" height="50px">
                            <asp:TextBox ID="txtName" CssClass="form-control" runat="server" MaxLength="50" Width="300px" />
                            <asp:RequiredFieldValidator ID="vldNameReq" ControlToValidate="txtName" runat="server"
                                Display="None" ErrorMessage="Name is required" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">DOB:
                        </td>
                        <td  height="50px">
                            <asp:TextBox ID="txtDOB" CssClass="form-control datetimepicker" runat="server" MaxLength="50" Width="300px" />

                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <span style="color: red;">*</span>&nbsp;Address:
                        </td>
                        <td height="50px">
                            <asp:TextBox ID="txtAddress" CssClass="form-control" runat="server" TextMode="MultiLine" MaxLength="100" Width="300px" />
                            <asp:RequiredFieldValidator ID="vldAddressReq" ControlToValidate="txtAddress" runat="server"
                                Display="None" ErrorMessage="Address is required" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <span style="color: red;">*</span>&nbsp;City:
                        </td>
                        <td height="50px">
                            <asp:TextBox ID="txtCity" CssClass="form-control" runat="server" MaxLength="50" Width="300px" />
                            <asp:RequiredFieldValidator ID="vldCityReq" ControlToValidate="txtCity" runat="server"
                                Display="None" ErrorMessage="City is required" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <span style="color: red;">*</span>&nbsp;State:
                        </td>
                        <td height="50px">
                            <asp:TextBox ID="txtState" CssClass="form-control" runat="server" MaxLength="50" Width="300px" />
                            <asp:RequiredFieldValidator ID="vldStateReq" ControlToValidate="txtState" runat="server"
                                Display="None" ErrorMessage="State is required" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Phone:
                        </td>
                        <td height="50px">
                            <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" MaxLength="50" Width="300px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <span style="color: red;">*</span>&nbsp;Email:
                        </td>
                        <td height="50px">
                            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" MaxLength="50" Width="300px" />
                            <asp:RequiredFieldValidator ID="vldEmailReq" ControlToValidate="txtEmail" runat="server"
                                Display="None" ErrorMessage="Email is required" />
                            <asp:RegularExpressionValidator ID="vldEmailReg" runat="server" ErrorMessage="Email is invalid"
                                Display="None" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

