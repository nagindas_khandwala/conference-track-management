﻿using System;

public partial class ui : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["User"] ==null) {
            Response.Redirect("default.aspx?page=" + Server.UrlEncode(Request.RawUrl));
        }            
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            if (Session["User"] != null)
            {
                Session["UserName"] = ((AdminUser)Session["User"]).Name;
                lblLoginUser.Text = "Welcome: <strong>" + ((AdminUser)Session["User"]).Name + "</strong>";
                lnkLogut.Visible = true;
            }
        }
    }
    
    protected void lnkLogut_Click(object sender, EventArgs e)
    {
        Session["User"] = null;
        Session["Auction"] = null;

        Response.Redirect("~/default.aspx");
    }

    protected void lnkMyProfile_Click(object sender, EventArgs e)
    {
        Response.Redirect("myprofile.aspx");
    }
}
